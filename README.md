# Ninepine Official Website
### Ninepine's website made using [Gridsome](https://gridsome.org/).

### Build Setup
**Install dependencies**
```
$ npm install
```

**Serve with hot reload**
```
$ npm run develop
```

**Build for production**
```
$ npm run build
```